# EMADL-Demo
This repo contains an example project for the language [EmbeddedMontiArcDL](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/languages/EmbeddedMontiArcDL).
It uses the generator [EMADL2CPP](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/generators/EMADL2CPP) to generate code from the EMADL model.
The demo project was only tested on Ubuntu16.04. It will not work on windows.
The shell script *setup.sh* installs all libraries which are required for the generated product. 
Note that it is necessary to install cuda and cudnn manually to use the gpu version of MxNet instead of the cpu version.

Use the script *make.sh* to build an application that can classify [cifar10](https://www.cs.toronto.edu/~kriz/cifar.html)-images based on the EMADL model in "src/emadl".
The script will first create HDF5 files based on the images stored in the directory "src/resources/data/train". 
All images of the same class are in the same folder and the name of this folder denotes the class index.
Then, the script will train a convolutional neural network based on the created HDF5 datasets.
The trained neural network is stored in the *models* folder.
Finally, it will compile the generated C++ code.

The complete application will be stored in the created folder with the name *target*.
The application can be executed by providing the path to an test image as argument. Test images are available in "src/resources/data/test".
Execute the script *run.sh* in the *target* folder to execute the image classifier on the image *img.png* in the same directory.
It will classify the image with the name *img.png* in the same directory.
Note that the shared library of MxNet has to be added to the LD_LIBRARY_PATH to be able to use the program without the script.
The output of the program is the predicted class for the input image.
The following 10 classes exist in the cifar10 dataset: 
  airplane(`0`),
  automobile(`1`),
  bird(`2`),
  cat(`3`),
  deer(`4`),
  dog(`5`),
  frog(`6`),
  horse(`7`),
  ship(`8`),
  truck(`9`).
