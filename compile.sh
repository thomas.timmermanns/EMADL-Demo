echo "Compiling image classifier.."
cp src/resources/ImageClassifier.cpp build/ImageClassifier.cpp
cd build
g++ -std=c++11 ImageClassifier.cpp -o ImageClassifier -I. -I$HOME/incubator-mxnet/include -L$HOME/incubator-mxnet/lib -lmxnet -llapack -lblas -larmadillo  `pkg-config --cflags --libs opencv`
echo "Creating target folder.."
cd ..
mkdir -p target/model/cifar10_main_net
cp build/model/cifar10_main_net/net_newest-symbol.json target/model/cifar10_main_net/net_newest-symbol.json
cp build/model/cifar10_main_net/net_newest-0000.params target/model/cifar10_main_net/net_newest-0000.params
cp build/ImageClassifier target/ImageClassifier
cp src/resources/run.sh target/run.sh
cp src/resources/data/test/7/test_batch.bin-13-7.png target/img.png
