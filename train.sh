source ~/mxnet/bin/activate
echo "Creating HDF5 dataset from image files.."
cd src/resources
tar xf data.tar.gz
cd ..
cd ..
python src/resources/imgDir-to-h5.py --in_port data --out_port softmax --data_path src/resources/data --target_path build/data/cifar10_main_net
echo "Start network training.."
cd build
python CNNTrainer_cifar10_Main.py
cd ..
